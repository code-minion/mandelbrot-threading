/**
 *	@file GDE\Tests\TestMandelbrot\TestMandelbrot.cpp
 *	@brief:
 *	
 *	Copyright()
 *		Copyright � 2004-2011 Media Design School. All Rights Reserved.
 *	
 *********************************************************************************************/

#include "GDE/Core/Core.h"
#include "GDE/Core/PerfTools.h"
#include "GDE/Renderer/Renderer.h"
#include "GDE/Renderer/Vertex.h"
#include "GDE/Renderer/FontManager.h"
#include "GDE/Renderer/Font.h"
#include "GDE/Core/GameTimer.h"

#include "GDE/Input/InputDevice.h"
#include "GDE/Input/InputChangeSubject.h"

#include "GDE/Renderer/Texture.h"
#include "GDE/Renderer/TextureManager.h"

#include "GDE/Core/DLLLoader.h"

#include "GDE/Core/Logger.h"
#include "GDE/Core/ConsoleLog.h"
#include "GDE/Core/DebuggerLog.h"

#include "TestMandelbrot.h"
#include "MandelbrotBuffer.h"
#include "KeyboardObserver.h"

#include "GDE/DataParser/DataParserManager.h"
#include "GDE/DataParser/DataParser.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <cmath>
#include <stdio.h>
#include <omp.h>
#include <vector>

#ifdef _DEBUG
const char* g_strDllName = "HGERenderer_D.dll";
const char* g_strInputDllName = "HGERenderer_D.dll"; 
const char* g_strFontDllName = "HGERenderer_D.dll";
#else
const char* g_strDllName = "HGERenderer.dll";
const char* g_strInputDllName = "HGERenderer.dll"; 
const char* g_strFontDllName = "HGERenderer.dll";
#endif

using namespace GDE;

/**
 *	Main windows application entry point.
 */
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	CRenderer TheRenderer;
	if ( false == TheRenderer.LoadDLL( g_strDllName ) )
	{
		printf( "Unable to load %s\n", g_strDllName );
		assert_now( "Unable to load %s\n", g_strDllName );
		return 0;
	}

	CMandelbrotContext RendererContext( TheRenderer );
	TheRenderer.SetContext( RendererContext );
	RendererContext.Init();

	TheRenderer.RunGameLoop();
	RendererContext.Release();

	return 0;
}


/**
 *	Constructor, prepare the context.
 */
CMandelbrotContext::CMandelbrotContext( CRenderer& Renderer )
: m_Renderer( Renderer )
, m_pInputKeyboard( NULL )
, m_pFrameTimer( NULL )
, m_pFontMgr( NULL )
, m_pFont( NULL )
, m_pMandelbrotTexture( NULL )
, m_pMandelbrot( NULL )
, m_uNumThreads(4)
{	
	m_bCreateZBuffer = false;

	m_pFrameTimer = new GDE::CClock();

	m_pFontMgr = new GDE::CFontManager();
	if ( false == m_pFontMgr->LoadDLL( g_strFontDllName, &m_Renderer ) )
	{
		printf( "Unable to load %s\n", m_pFontMgr );
		assert_now( "Unable to load %s\n", m_pFontMgr );
	}

	m_pInputKeyboard = new GDE::CInputDevice();
	if ( false == m_pInputKeyboard->LoadDLL( g_strInputDllName ) )
	{
		printf( "Unable to load %s\n", g_strDllName );
		assert_now( "Unable to load %s\n", g_strDllName );
	}

	//Register keys:
	const char* strKeyCommands[] = 
	{
		"KeyEscape", //KC_Quit,		
	};
	_COMPILE_ASSERT( _ARRAY_SIZE( strKeyCommands ) == KC_Count );

	for ( GDE::UINT32 i=0; i<KC_Count; i++ )
	{
		m_bKeyDown[i] = m_bKeyWasDown[i] = false;
		m_KeyObserver[i].Init( m_bKeyDown[i] );
		CreateAndRegisterKey( 
			strKeyCommands[i], 
			m_pInputKeyboard->GetChangeSubject(),
			m_KeyObserver[i] );
	}

	// Create the data parser
	m_pParserManager = new GDE::CDataParserManager();
	if ( false == m_pParserManager->LoadDLL( g_strDllName ) )
	{
		printf( "Unable to load %s\n", g_strDllName );
		assert_now( "Error loading dll \"%s\"", g_strDllName );
	}
}

/**
 *	Initialise the context, called once the renderer has been created and initialised.
 */
void CMandelbrotContext::Init( void )
{
	// Load Granularity, # Threads, etc.
	CDataParser* pParser = m_pParserManager->LoadResource("ThreadingConfig.ini");

	assert_msg( NULL != pParser, "Unable to open mandelbrot template image.");

	pParser->GetValue( m_uNumThreads, "Thread", "NumThreads" );
	pParser->GetValue( m_uGranularity, "Thread", "Granularity" );
	pParser->Release();

	omp_set_num_threads(m_uNumThreads);

	//TODO: tacky, really need a way to create blank images - not render tagets though!
	m_pMandelbrotTexture = m_Renderer.GetTextureManager().LoadResource(
		GDE::CResourceId( "MandelbrotTemplate.png" ) );
	assert_msg( NULL != m_pMandelbrotTexture, "Unable to open mandelbrot template image." );

	m_pMandelbrot = new CMandelbrotBuffer(
		m_pMandelbrotTexture->GetWidth(), m_pMandelbrotTexture->GetHeight(),
		m_pMandelbrotTexture->GetWidth(), m_pMandelbrotTexture->GetHeight(), m_uNumThreads, m_uGranularity );


	// create the debug font
	m_pFont = m_pFontMgr->LoadResource( CResourceId( "CourierNew.fnt" ) );
}


/**
 *	Register's a key to the given event with the keyboard input.
 */
void CMandelbrotContext::CreateAndRegisterKey( 
		const char* strKey, 
		GDE::IInputChangeSubject& Subject, 
		IObserver& Observer )
{
	//Register the observers
	GDE::UINT32 uCode;
	if ( m_pInputKeyboard->GetInputCode( strKey, uCode ) )
	{
		Subject.AddObserver( (GDE::IObserver*)&Observer, uCode );
	} else
	{
		assert_now( "Unable to bind input for key \"%s\"", strKey );
	}
}

/**
 *	Destructor.
 */
CMandelbrotContext::~CMandelbrotContext( void )
{
	SafeDelete( m_pInputKeyboard );
	SafeDelete( m_pFrameTimer );
	SafeDelete( m_pFontMgr );
}

/**
 *	Release all the context's members, called before the renderer is shut down.
 */
void CMandelbrotContext::Release( void )
{
	SafeRelease( m_pFont );
	SafeRelease( m_pMandelbrotTexture );
	SafeDelete( m_pMandelbrot );
}


/**
 *	Update the keyboard states for the frame.
 */
void CMandelbrotContext::UpdateKeyStates( void )
{
	memcpy( m_bKeyWasDown, m_bKeyDown, sizeof( m_bKeyWasDown ) );
}


/**
 *	Implements the basic context requirements, update and then render the frame.
 */
bool CMandelbrotContext::FrameUpdate( void )
{
	m_pFrameTimer->FrameUpdate();
	
	FLOAT32 fTimeInterval = m_pFrameTimer->GetFrameInterval(); // allows pausing.

//	if ( m_Renderer.IsActiveWindow() )
	{
		m_pInputKeyboard->Update( fTimeInterval );
	}
	UpdateKeyStates();

	FrameRender();

	m_pMandelbrot->Fill();
	UpdateTexture();

	return m_bKeyDown[KC_Quit] == false;
}

void CMandelbrotContext::FrameRender()
{
	m_Renderer.BeginScene();
	m_Renderer.ColourClear( 0xFF000000 );

	m_pMandelbrotTexture->Apply( 0 );
	const static SVertex Quads[] = 
	{
		// x,		y,		z,		colour,		u,	v
		{ 0.0f,	0.0f,	0.0f, 0xFFFFFFFF, 0.0f, 0.0f },
		{ FLOAT32(m_uDispWidth),	0.0f,	0.0f, 0xFFFFFFFF, 1.0f, 0.0f }, 
		{ FLOAT32(m_uDispWidth),	FLOAT32(m_uDispHeight),	0.0f, 0xFFFFFFFF, 1.0f, 1.0f }, 
		{ 0.0f,	FLOAT32(m_uDispHeight),	0.0f, 0xFFFFFFFF, 0.0f, 1.0f }, // end quad
	};

	m_Renderer.DrawPrimitives( PT_QuadList, _ARRAY_SIZE(Quads)/4, Quads );
	m_Renderer.GetTextureManager().GetNULLTexture()->Apply( 0 ); 

	// Draw framerate text
	char InfoText[256];
	const FLOAT32 fFrameRate = 1.0f/m_pFrameTimer->GetFrameInterval();
	const GDE::UINT32 uFrameRate = GDE::UINT32(fFrameRate);
	const GDE::UINT32 uFrameDecimal = GDE::UINT32(fFrameRate*100.0f)%100;


	sprintf_s(  InfoText, "Framerate %05d.%02d", uFrameRate, uFrameDecimal );
	m_pFont->Render( 10, 580, GDE::FJ_Left, InfoText );

	// Debug info
	sprintf_s(  InfoText, "Threads : %i", m_uNumThreads);
	m_pFont->Render( 180, 580, GDE::FJ_Left, InfoText );

	// Text out Granularity
	sprintf_s(  InfoText, "Granularity : %i", m_uGranularity);
	m_pFont->Render( 300, 580, GDE::FJ_Left, InfoText );
	
	// Text out Total Chunks
	sprintf_s(  InfoText, "Image Chunks : %i", m_pMandelbrotTexture->GetHeight()/m_uGranularity);
	m_pFont->Render( 450, 580, GDE::FJ_Left, InfoText );

	// Text out image segments
	//sprintf_s(  InfoText, "Chunks Remaining : %i", m_pMandelbrot->m_uChunksDone);
	//m_pFont->Render( 610, 580, GDE::FJ_Left, InfoText );

	m_Renderer.EndScene();
}

/**
 *	Update the texture from the image created.
 */
void CMandelbrotContext::UpdateTexture( void )
{
	GDE::UINT32* pTexels = NULL;
	GDE::UINT32 uPitch;
	m_pMandelbrotTexture->LockTexels( pTexels, uPitch, false );

	// fast and nasty:
//	memcpy( pTexels, m_pMandelbrot->GetPixels(), uPitch* m_pMandelbrotTexture->GetHeight() );

	for ( int uRow = 0; uRow<(int)m_pMandelbrotTexture->GetHeight(); uRow++ )
	{
//#pragma omp parallel for
		for ( int uCol = 0; uCol<(int)m_pMandelbrotTexture->GetWidth(); uCol++ )
		{
			pTexels[uCol] = m_pMandelbrot->TexelAt( uCol, uRow );
		}
		pTexels += uPitch;
	}

	m_pMandelbrotTexture->UnlockTexels();
}

