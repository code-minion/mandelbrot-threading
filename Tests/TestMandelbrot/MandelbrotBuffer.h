/**
 *	@file Mandelbrot/Headers/MandelbrotBuffer.h
 *	@brief
 *
 *	Copyright()
 *		Copyright � 2009-2010 Media Design School. All Rights Reserved.
 *
 *	@author Jon Booth
 *
 *********************************************************************************************/

#ifndef _MANDELBROTBUFFER_H__Included_79333008
#define _MANDELBROTBUFFER_H__Included_79333008

/**
 *	This constructs the madelbrot image.
 *
 *	@author Jon Booth.
 */
class CMandelbrotBuffer
{
public:
	typedef GDE::UINT32 UINT32;
	typedef GDE::FLOAT32 FLOAT32;
	CMandelbrotBuffer( UINT32 uWidth, UINT32 uHeight, UINT32 uTexWidth, UINT32 uTexHeight, UINT32 uNumThreads, UINT32 uGranularity );
	~CMandelbrotBuffer( void );

	void SetMaxIterations( UINT32 uNumIterations );
	void Fill( void );

	const UINT32& TexelAt( UINT32 uX, UINT32 uY ) const
	{
		return m_pBuffer[uY*m_uTexWidth + uX];
	}

	UINT32& TexelAt( UINT32 uX, UINT32 uY )
	{
		return m_pBuffer[uY*m_uTexWidth + uX];
	}


	const UINT32* GetPixels( void ) const
	{
		return m_pBuffer;
	}

	void Clean();

public:
	volatile UINT32 m_uChunksDone;

protected:
	void Fill( UINT32 chunk );
	UINT32 CalculateForPoint( FLOAT32 fX, FLOAT32 fY ) const;

	static UINT32 HSVMake( FLOAT32 fH, FLOAT32 fS, FLOAT32 fV );
	static UINT32 MakeColF( FLOAT32 fR, FLOAT32 fG, FLOAT32 fB );

	UINT32	m_uMaxIterations;
	UINT32*	m_pPalette;
	UINT32	m_uWidth;
	UINT32	m_uHeight;
	UINT32	m_uMaxChunks;
	UINT32	m_uGranularity;
	UINT32	m_uNumThreads;

	// for the underlying texture.
	UINT32	m_uTexWidth;
	UINT32	m_uTexHeight;
	UINT32*	m_pBuffer;

	// below are the parameters for the x, y, width and height...
	FLOAT32 m_fX;
	FLOAT32 m_fY;
	FLOAT32 m_fWidth;
	FLOAT32 m_fHeight;
	FLOAT32 m_fXRatio;
	FLOAT32 m_fYRatio;
};


#endif //_MANDELBROTBUFFER_H__Included_79333008

