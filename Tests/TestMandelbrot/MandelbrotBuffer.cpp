/**
 *	@file Mandelbrot/MandelbrotBuffer.cpp
 *	@brief
 *
 *	Copyright()
 *		Copyright � 2009-2010 Media Design School. All Rights Reserved.
 *
 *	@author Jon Booth
 *
 *********************************************************************************************/
#include "GDE/Core/Core.h"
#include "MandelbrotBuffer.h"
#include <omp.h>
#include <cstring>
#include <windows.h>

/**
 * Construct and fill the Madelbrot buffer.
 *
 *	@author Jon Booth
 */
CMandelbrotBuffer::CMandelbrotBuffer( UINT32 uWidth, UINT32 uHeight, UINT32 uTexWidth, UINT32 uTexHeight, UINT32 uNumThreads, UINT32 uGranularity )
: m_uMaxIterations( 0 ) // is filled in later
, m_pPalette( NULL )
, m_uWidth( uWidth )
, m_uHeight( uHeight )
, m_uTexWidth( uTexWidth )
, m_uTexHeight( uTexHeight )
, m_fX( -2.0f )
, m_fY( -1.0f )
, m_fWidth( 3.0f )
, m_fHeight( 2.0f )
, m_uChunksDone ( 0 )
{
	m_fXRatio = m_fWidth/FLOAT32( m_uWidth );
	m_fYRatio = m_fHeight/FLOAT32( m_uHeight );
	m_pBuffer = new UINT32[uTexWidth*uTexHeight];
	m_uGranularity = uGranularity;
	//m_uNumThreads = 1;
	uNumThreads;
	m_uMaxChunks = uTexHeight/uGranularity;
	// build the palette at the highest point you can.
	SetMaxIterations( 256 );
	Fill();
}

CMandelbrotBuffer::~CMandelbrotBuffer( void )
{
	GDE::SafeDelete ( m_pPalette );
	GDE::SafeDelete( m_pBuffer );
}

/**
 *	Set the maximum number of iterations and build the palette for colours from that.
 *
 *	@author Jon Booth
 */
void CMandelbrotBuffer::SetMaxIterations( UINT32 uNumIterations )
{
	if ( uNumIterations == m_uMaxIterations )
	{
		return;
	}
	m_uMaxIterations = uNumIterations;
	if ( NULL != m_pPalette )
	{
		delete[] m_pPalette;
	}
	m_pPalette = new UINT32[m_uMaxIterations+1];
	static FLOAT32 fSat = 1.0f;
	static FLOAT32 fVal = 1.0f;
	const FLOAT32 fIterRatio = 1.0f/FLOAT32(m_uMaxIterations);
	for ( UINT32 i=0; i<m_uMaxIterations; i++ )
	{
		m_pPalette[i] = HSVMake( FLOAT32(i) * fIterRatio, fSat, fVal );
	}
	m_pPalette[m_uMaxIterations] =  MakeColF( 0.0f, 0.0f, 0.0f ); // black
}

void CMandelbrotBuffer::Clean()
{
	// Clear buffer
	memset(m_pBuffer, 0xFF000000, m_uWidth*m_uHeight*sizeof(UINT32));

}

/**
 *	Fill for the current window onto the Mandelbrot plane.
 *
 *	@author Jon Booth
 */
void CMandelbrotBuffer::Fill( void )
{		
	Clean();

	 #pragma omp parallel for
	for (int chunk = 0; chunk < (int)m_uMaxChunks; ++chunk)
	{
		Fill(chunk);
	}
}

/**
 *	Fill for the given window onto the Mandelbrot plane.
 *
 *	@author Jon Booth
 */
void CMandelbrotBuffer::Fill( UINT32 chunk_index )
{
	UINT32 uIterations;	

//	figure out initial value of int y with ( chunk_index * m_uGranularity )
	UINT32 uStartRow = chunk_index * m_uGranularity;

//	max value of Y is (( chunk_index + 1 ) * granularity )
	UINT32 uEndRow = (chunk_index + 1) * m_uGranularity;

	if (uEndRow > m_uHeight)
	{
		uEndRow = m_uHeight;
	}


	for ( UINT32 y = uStartRow ; y < uEndRow ; ++y )
	{
		for ( int x=0; x<(int)m_uWidth; x++ )
		{
			uIterations = CalculateForPoint( m_fX+FLOAT32(x)*m_fXRatio, m_fY+FLOAT32(y)*m_fYRatio );

			TexelAt( x, y ) = m_pPalette[uIterations];
		}
	}
	//++m_uChunksDone;
}

/**
 *	Create a colour from a given Hue Saturation and Value.
 *
 *	@author Jon Booth
 */
GDE::UINT32 CMandelbrotBuffer::HSVMake( FLOAT32 fH, FLOAT32 fS, FLOAT32 fV )
{
	 // H is given on [0, 1] or UNDEFINED. S and V are given on [0, 1].
	fH *= 6.0f;
	// RGB are each returned on [0, 1].
	FLOAT32 m, n, f;
	int i;
	
	i = int(fH);
	f = fH - i;
	if ( !(i&1) )
	{ 
		f = 1.0f - f; // if i is even
	}
	m = fV * (1.0f - fS);
	n = fV * (1.0f - fS * f);
	switch (i) 
	{
		case 6:
		case 0: return MakeColF(fV, n, m);
		case 1: return MakeColF(n, fV, m);
		case 2: return MakeColF(m, fV, n);
		case 3: return MakeColF(m, n, fV);
		case 4: return MakeColF(n, m, fV);
		case 5: return MakeColF(fV, m, n);
	}
	return MakeColF( fV, fV, fV );
}

/**
 *	Create a colour from floating point Red, Green and Blue values.
 *
 *	@author Jon Booth
 */
GDE::UINT32 CMandelbrotBuffer::MakeColF( FLOAT32 fR, FLOAT32 fG, FLOAT32 fB )
{
	return 0xFF000000|(UINT32(fR*255.0f)<<16)|(UINT32(fG*255.0f)<<8)|UINT32(fB*255.0f);
}


/**
 *	Calculate a given number of iterations for a given point
 */
GDE::UINT32 CMandelbrotBuffer::CalculateForPoint( FLOAT32 fX, FLOAT32 fY ) const
{
	const FLOAT32 fX0 = fX;
	const FLOAT32 fY0 = fY;
	fX = fY = 0.0f;
	FLOAT32 fT;
	for ( int i=0; i<(int)m_uMaxIterations; i++ )
	{
		if ( fX*fX+fY*fY > 4.0f )
		{
			return i;
		}
		fT = fX*fX - fY*fY + fX0;
		fY = 2.0f*fX*fY + fY0;
		fX = fT;
	}
	return m_uMaxIterations;
}
